#!/usr/bin/env groovy

def deployConfig
def mqttConnectors
def dbConnectors
def bindingVarArray
def moduleArray
def notifyLINE(status) {
    def token = "PVBVro4863TX772Z4yQdm9jROEN2tTMdenkUhNwbFqg"
    //def jobName = env.JOB_NAME + ' ' + env.BRANCH_NAME
    def jobName = env.JOB_NAME
    def buildNo = env.BUILD_NUMBER
      
    def url = 'https://notify-api.line.me/api/notify'
    def message = "${jobName} Build #${buildNo} :${jiraid} ${status} \r\n"
    sh "curl ${url} -H 'Authorization: Bearer ${token}' -F 'message=${message}'"
}


pipeline {
    agent any
    options {
        // using the Timestamper plugin we can add timestamps to the console log
        timestamps()
        // Set timeout for build pipeline job.
        timeout(time: 20, unit: 'MINUTES')
    } 

    parameters {
        string(name: 'MODULE_CONFIG', defaultValue: 'msa-dmabuilder', description: 'Module Configuration Ex. training-msa-go-tong')
    }

    stages {

        stage('Prepare Workspace Directory') {
            steps {
                echo "Home workspace path : ${env.WORKSPACE}"
                sh "rm -rf ${env.WORKSPACE}/*"
                //sh "rm -rf ${env.WORKSPACE}/db-factory"
                //sh "rm -rf ${env.WORKSPACE}/benthos"
            }
        }

        stage('Git Clone Common Pipeline Module') {
            steps {
                sh "git clone -b master --single-branch --depth 1 ssh://git@bitbucket.beebuddy.net:7999/wdscompany-assets/msa-pipeline-common.git ${env.WORKSPACE}/common"
            }
        }

        stage('Git Clone Data-Factory Template') {
            steps {
                sh "git clone -b 'feature/SSQ-794-ssq-analytic-accesslog-data-factory' --single-branch --depth 1 ssh://git@bitbucket.beebuddy.net:7999/anc/java-apig-accesslog-api.git ${env.WORKSPACE}/data-factory-template"
                sh "rm -rf ${env.WORKSPACE}/data-factory-template/mappers"
                sh "rm -rf ${env.WORKSPACE}/data-factory-template/.git"
                sh "rm -rf ${env.WORKSPACE}/data-factory-template/README.md"      
            }
        }

        stage('Git Clone Config') {
            steps {
                sh "git clone -b 'master' --single-branch --depth 1 ssh://git@bitbucket.beebuddy.net:7999/cus/deves-data-factory.git ${env.WORKSPACE}/deploy_config"
            }
        }

        stage('Create Profile Folder') {
            steps {
                script { 
                        def files = null
                        sh "mkdir ${env.WORKSPACE}/profiles"
                        profileInput = readJSON file: "${env.WORKSPACE}/deploy_config/input.json"
                        profileArray = profileInput['target']
                        for(profileVar in profileArray) {
                            def profileName = profileVar.targetDir                         
                            println("Found profile " + profileName)
                            sh "cp -r ${env.WORKSPACE}/data-factory-template ${env.WORKSPACE}/profiles/${profileName}"
                        }

                        sh "${env.WORKSPACE}/common/execute/bind-all  -templateFile=${env.WORKSPACE}/data-factory-template/application.properties -jsonFile=${env.WORKSPACE}/deploy_config/input.df"

                        for(profileVar in profileArray) {
                            def profileName = profileVar.targetDir                         
                            sh "cp ${profileName} ${env.WORKSPACE}/profiles/${profileName}/application.properties"
                        }
                }
            }
        }

        stage('Pack Files') {
            steps {
                ws("${env.WORKSPACE}/profiles"){
                      sh "tar -zcvf ssq-collector-profile.tar.gz *"                  
                }
            }
        }

        stage('Transfer Microserver To Remote Host') {
            steps {
                sh "scp ${env.WORKSPACE}/profiles/ssq-collector-profile.tar.gz consoleadmin@ssq-analytic-uat.beebuddy.net:/tmp/ssq-collector-profile.tar.gz"
                sh "ssh consoleadmin@ssq-analytic-uat.beebuddy.net sudo rm -rf /opt/stack/me-gateway/datafactory/data/mappers/*"
                sh "ssh consoleadmin@ssq-analytic-uat.beebuddy.net sudo tar -zxvf /tmp/ssq-collector-profile.tar.gz -C /opt/stack/me-gateway/datafactory/data/mappers/"
            }
        }
    }
}